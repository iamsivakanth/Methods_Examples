package com.training.paramethods;

public class CallByRef {
	private String name1 = "";
	private String name2 = new String("");
	private ValueBean bean = new ValueBean();

	public void perform() {
		bean.setA(10);
		bean.setB(20);
		test1(name1, name2, bean);
		display();
	}

	private void test1(String s1, String s2, ValueBean bean) {
		s1 = "Java";
		s2 = "Spring";
		bean.setA(100);
		bean.setB(200);

		System.out.println("S1 : " + s1);// Java
		System.out.println("S2 : " + s2);// Spring
		System.out.println("Values of Bean");
		System.out.println(bean.getA() + " , " + bean.getB());// 100, 200
	}

	private void display() {
		System.out.println("Name1 : " + name1);// ""
		System.out.println("Name2 : " + name2);// ""
		System.out.println("Values of Bean");
		System.out.println(bean.getA() + " , " + bean.getB());// 100, 200
	}
}