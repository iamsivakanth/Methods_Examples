package com.training.paramethods;

public class ParaMethodsExe {
	private double basicSal;
	private double hra;// 10% of net sal
	private double increment;
	private double totSal;

	public void calculate(double basicSal, double increment) {// Formal parameters
		this.basicSal = basicSal;
		this.increment = increment;
		hra = (basicSal + increment) * 10 / 100;
		totSal = basicSal + increment + hra;
		display();
	}

	private void display() {
		System.out.println("Basic Salary : " + basicSal);
		System.out.println("HRA : " + hra);
		System.out.println("Increment : " + increment);
		System.out.println("Total Salary : " + totSal);
	}
}