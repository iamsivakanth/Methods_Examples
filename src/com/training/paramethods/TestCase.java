package com.training.paramethods;

public class TestCase {

	private int a = 10;

	private void test1() {
		test2(a);
		display();
	}

	private void test2(int a) {
		a = a * a;
		System.out.println("Param of test2() : " + a);
	}

	private void display() {
		System.out.println("Param of display() : " + a);
	}

	public static void main(String[] args) {
		TestCase case1 = new TestCase();
		case1.test1();
	}
}