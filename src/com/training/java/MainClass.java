package com.training.java;

import com.training.method.MethodExe;
import com.training.method.StaticMethodExe;
import com.training.paramethods.ParaMethodsExe;

public class MainClass {

	public static void main(String[] args) {
//		MethodExe exe = new MethodExe();
//		exe.setA(10);
//		exe.setB(20);
//		exe.addCall();

//		StaticMethodExe.setA(50);
//		StaticMethodExe.setB(700);
//		StaticMethodExe.addCall();

		ParaMethodsExe methodsExe = new ParaMethodsExe();
		methodsExe.calculate(65000.0, 10000.0);// Actual Parameters
	}
}