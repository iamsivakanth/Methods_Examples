package com.training.returntype;

public class ReturnTypeExe {
	private int day;

	public int findDay(String week) {
		if (week == null) {
			return day;
		}
		
		week = week.toUpperCase();
		week = week.trim();
		switch (week) {
		case "MON":
			day = 1;
			break;
		case "TUE":
			day = 2;
			break;
		case "WED":
			day = 3;
			break;
		case "THU":
			day = 4;
			break;
		case "FRI":
			day = 5;
			break;
		case "SAT":
			day = 6;
			break;
		case "SUN":
			day = 7;
			break;
		default:
			System.out.println("Invalid week");
		}
		return day;
	}
}