package com.training.method;

public class StaticMethodExe {

	private StaticMethodExe() {
	}

	private static int a, b, c;

	public static void addCall() {
		addition();
	}

	public static void setA(int a) {
		StaticMethodExe.a = a;
	}

	public static void setB(int b) {
		StaticMethodExe.b = b;
	}

	private static void addition() {
		c = a + b;
		System.out.println("Addition : " + c);
	}
}
