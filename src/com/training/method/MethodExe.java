package com.training.method;

public class MethodExe {
	private int a, b, c;

	public void addCall() {
		addition();
	}

	private void addition() {
		c = a + b;
		System.out.println("Addition : " + c);
	}

	public void setA(int a) {
		this.a = a;
	}

	public void setB(int b) {
		this.b = b;
	}
}